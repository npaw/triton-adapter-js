var youbora = require('youboralib')
var manifest = require('../../manifest.json')

var AdsAdapter = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-Triton-ads'
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.duration
  },

  getTitle: function () {
    return this.title
  },

  /** Override to return current ad position (only ads) */
  getPosition: function () {
    var ret = youbora.Constants.AdPosition.Midroll
    if (!this.plugin.getAdapter().flags.isJoined) {
      ret = youbora.Constants.AdPosition.Preroll
    }
    return ret
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Register listeners
    this.references = {
      'ad-playback-start': this.playListener.bind(this),
      'ad-playback-complete': this.endedListener.bind(this),
      'ad-playback-destroy': this.endedListener.bind(this),
      'ad-countdown': this.timeUpdateListener.bind(this),
      'ad-break-cue-point': this.cuePointListener.bind(this)
    }

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  playListener: function (e) {
    this.plugin.fireInit()
    this.plugin.getAdapter().firePause()
    if (!this.flags.isStarted) {
      this.duration = null
      this.playhead = 0
      this.firstQuartileTriggered = false
      this.secondQuartileTriggered = false
      this.thirdQuartileTriggered = false
      this.fireStart()
    }
  },

  endedListener: function (e) {
    this.fireStop()
  },

  timeUpdateListener: function (e) {
    var countdown = e.data.countDown
    if (!this.duration) this.duration = countdown
    this.playhead = this.duration - countdown
    this.fireJoin()
    this.quartileManager()
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.player.ads && this.references) {
      for (var key in this.references) {
        this.player.ads.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  quartileManager: function () {
    if (this.getPlayhead() > this.getDuration() / 4) {
      if (!this.firstQuartileTriggered) {
        this.fireQuartile(1)
        this.firstQuartileTriggered = true
      } else if (this.getPlayhead() > this.getDuration() / 2) {
        if (!this.secondQuartileTriggered) {
          this.fireQuartile(2)
          this.secondQuartileTriggered = true
        } else if (this.getPlayhead() > this.getDuration() * 0.75) {
          if (!this.thirdQuartileTriggered) {
            this.fireQuartile(3)
            this.thirdQuartileTriggered = true
          }
        }
      }
    }
  },
  cuePointListener: function (e) {
    if (e && e.adBreakData) {
      this.title = e.adBreakData.cueTitle
    }
  }
})

module.exports = AdsAdapter
