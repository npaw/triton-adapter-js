var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Triton = youbora.Adapter.extend({

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.playhead
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.duration
  },

  /** Override to return title */
  getTitle: function () {
    return this.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return this.duration === 0
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.src
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player.version.value
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Triton player'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    /* var events = [
      null,
      'track-cue-point',
      'stream-track-change',
      'hls-cue-point',
      'speech-cue-point',
      'custom-cue-point',
      'stream-status',
      'npe-song',
      'stream-select',
      'list-loaded',
      'list-empty',
      'pwa-data-loaded',
      'vpaid-ad-companions',
      'ad-break-cue-point',
      'ad-break-cue-point-complete',
      'pwa-data-loaded',
      'vpaid-ad-companions',
      'ad-countdown',
      'ad-quartile',
      'vpaid-ad-companions',
      'video-mid-roll-playback-start',
      'video-mid-roll-playback-complete',
      'ad-break-synced-element'] */

    // Console all events if logLevel=DEBUG
    // youbora.Util.logAllEvents(this.player, events)

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true, 1250)

    // References
    this.references = {
      'player-ready': this.readyListener.bind(this),
      'stream-start': this.playListener.bind(this),
      'media-playback-status': this.statusListener.bind(this),
      'stream-status': this.streamStatusListener.bind(this),
      'stream-stop': this.endedListener.bind(this),
      'media-playback-timeupdate': this.timeupdateListener.bind(this),
      'configuration-error': this.errorListener.bind(this),
      'module-error': this.errorListener.bind(this),
      'stream-geo-blocked': this.errorListener.bind(this),
      'media-error': this.errorListener.bind(this),
      'track-cue-point': this.cuePointListener.bind(this),
      'speech-cue-point': this.cuePointListener.bind(this)
    }
    // Register listeners
    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  readyListener: function (e) {
    if (!this.plugin.getAdsAdapter()) {
      this.plugin.setAdsAdapter(new youbora.adapters.Triton.AdsAdapter(this.player))
    }
  },

  /** Listener for 'stream-start' event. */
  playListener: function (e) {
    this.fireStart()
  },

  /** Listener for 'media-playback-timeupdate' event. */
  timeupdateListener: function (e) {
    if (e.data) {
      this.duration = isNaN(e.data.duration) ? 0 : e.data.duration
      this.playhead = e.data.playedTime
    }
    if (!this.flags.isStarted) {
      this.monitor.stop()
      this.fireStart()
      this.fireJoin()
      this.monitorPlayhead(true, true, 1250)
    }
  },

  streamStatusListener: function (e) {
    if (e.data) {
      switch(e.data.code) {
        case 'LIVE_CONNECTING':
        case 'LIVE_BUFFERING':
          this.fireBufferBegin()
          this.fireStart()
          break
        case 'LIVE_PLAYING':
          this.fireJoin()
          this.monitor.stop()
          this.fireBufferEnd()
          break
        case 'GETTING_STATION_INFORMATION':
        case 'LIVE_STOP':
          this.fireStop()
          break
        case 'LIVE_FAILED':
        case 'STATION_NOT_FOUND':
        case 'PLAY_NOT_ALLOWED':
        case 'STREAM_GEO_BLOCKED':
          var errorCode = (e.data.status || e.data.code)
          var errorMsg = (e.data.message || e.data.status)
          try {
            // Get ErrorCode from message, if have pattern `ERROR_CODE: Message`,
            //  because status or code don't have a lot of information
            if (errorMsg.indexOf(':') > 0) {
              errorCode = errorCode + ': ' + errorMsg.substring(0, errorMsg.indexOf(':')).trim()
              errorMsg = errorMsg.substring(errorMsg.indexOf(':') + 1).trim()
            }
          } catch (e) {
            errorCode = (e.data.status || e.data.code)
            errorMsg = (e.data.message || e.data.status)
          }
          this.fireFatalError(errorCode, errorMsg)
          break
      }
    }
  },

  /** Listener for 'media-playback-status' event. */
  statusListener: function (e) {
    if (e.data) {
      if (e.data.mediaElement) {
        this.src = e.data.mediaElement.src
        this.duration = isNaN(e.data.mediaElement.duration) ? 0 : e.data.mediaElement.duration
        this.playhead = e.data.mediaElement.currentTime
      }
      switch (e.data.status) {
        case 'Buffering':
          if (!this.flags.isStarted) {
            this.playhead = 0
            this.fireStart()
          }
          this.skipNext = false
          break
        case 'Playing':
          if (!this.flags.isStarted) {
            this.timeupdateListener()
          }
          this.fireJoin()
          if (this.flags.isPaused || this.flags.isSeeking) {
            this.fireResume()
            this.monitor.skipNextTick()
          }
          if (this.skipNext) this.monitor.skipNextTick()
          this.skipNext = true
          break
        case 'Stopped':
          if (this.flags.isJoined) {
            this.fireStop()
          }
          break
        case 'Paused':
          this.firePause()
          break
      }
    }
  },

  /** Listener for error events. */
  errorListener: function (e) {
    this.fireError()
  },

  /** Listener for 'stream-stop' event. */
  endedListener: function (e) {
    this.fireStop()
  },

  cuePointListener: function (e) {
    if (e && e.data && e.data.cuePoint) {
      this.title = e.data.cuePoint.title || e.data.cuePoint.cueTitle
    }
  }
},
{
  AdsAdapter: require('./ads/adsadapter')
}
)

module.exports = youbora.adapters.Triton
