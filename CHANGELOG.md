## [6.8.4] 2024-11-13
### Library
- Packaged with `lib 6.8.59`

## [6.8.3] 2022-10-20
### Added
- Capture fired error for status `PLAY_NOT_ALLOWED` and `STREAM_GEO_BLOCKED`
- Close previous view with handle player status `GETTING_STATION_INFORMATION`
- Split some error messages to enrich error code and have only error details on error msg value
### Library
- Packaged with `lib 6.8.31`

## [6.8.2] 2022-01-18
### Added 
 - Additional information for errors: `LIVE_FAILED` and `STATION_NOT_FOUND` cases. Now it can report the message.
### Library
- Packaged with `lib 6.8.11`

## [6.8.1] 2021-08-31
### Fixed 
 - Buffer wrong detection when using `stream-status` listener

## [6.8.0] 2021-08-16
### Added 
 - `stream-status` event listener
### Library
- Packaged with `lib 6.8.0` 

## [6.7.0] 2020-11-12
### Library
- Packaged with `lib 6.7.22`
